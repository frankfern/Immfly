from django.urls import path
from .views import ChannelViewset, ContentViewset

from rest_framework import routers

router = routers.SimpleRouter()
router.register('channels', ChannelViewset, basename='channels')
router.register(
    r'channels/(?P<channel_id>[0-9]+)/contents', ContentViewset, basename='contents')

urlpatterns = router.urls
