from rest_framework import serializers
from .models import Channel, File, Content


class ChannelSerializer(serializers.ModelSerializer):

    class Meta:
        model = Channel
        fields = ['id', 'title', 'language', 'picture',
                  'parent_channel', 'has_subchannels', 'rating']


class FileSerializer(serializers.ModelSerializer):
    class Meta:
        model = File
        fields = ['id', 'file_type', 'url', 'content']


class ContentSerializer(serializers.ModelSerializer):
    files = FileSerializer(read_only=True, many=True)

    class Meta:
        model = Content
        fields = ['id', 'title', 'description', 'metadata',
                  'rating', 'channel', 'files']
