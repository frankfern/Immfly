# Generated by Django 4.2 on 2023-04-10 09:08

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('channels', '0003_alter_content_rating'),
    ]

    operations = [
        migrations.AlterField(
            model_name='content',
            name='channel',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='contents', to='channels.channel'),
        ),
        migrations.AlterField(
            model_name='content',
            name='description',
            field=models.TextField(null=True),
        ),
        migrations.AlterField(
            model_name='content',
            name='metadata',
            field=models.JSONField(default=dict, null=True),
        ),
        migrations.AlterField(
            model_name='content',
            name='rating',
            field=models.DecimalField(decimal_places=1, default=None, max_digits=3, null=True),
        ),
    ]
