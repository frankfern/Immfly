from django.test import TestCase
from django.core.management import call_command
from channels.models import Channel, Content
from channels.utils import get_channel_rating


class ExportChannelRatingsTests(TestCase):

    def setUp(self):
        self.channel1 = Channel.objects.create(title='Channel 1')
        self.channel2 = Channel.objects.create(title='Channel 2')
        self.content1 = Content.objects.create(
            title='Content 1', rating=4, channel=self.channel1)
        self.content2 = Content.objects.create(
            title='Content 2', rating=5, channel=self.channel1)
        self.subchannel = Channel.objects.create(
            title='Subchannel', parent_channel=self.channel2)
        self.content3 = Content.objects.create(
            title='Content 3', rating=3, channel=self.subchannel)

    def test_calculate_channel_rating_with_contents_only(self):
        channel = self.channel1
        expected_rating = (self.content1.rating + self.content2.rating) / 2
        self.assertEqual(get_channel_rating(
            channel), expected_rating)

    def test_calculate_channel_rating_with_subchannels(self):
        channel = self.channel2
        expected_rating = self.content3.rating
        self.assertEqual(get_channel_rating(
            channel), expected_rating)

    def test_calculate_channel_rating_with_subchannels_and_subchannels_contents(self):
        channel = self.channel1
        expected_rating = (self.content1.rating +
                           self.content2.rating) / 2
        self.assertEqual(get_channel_rating(
            channel), expected_rating)

    def test_export_channel_ratings(self):
        call_command('calculate_channel_ratings')
        with open('channel_ratings.csv', mode='r') as file:
            lines = file.readlines()
            self.assertEqual(lines[0], 'Channel Title,Average Rating\n')
            self.assertEqual(
                lines[1], f'{self.channel1.title},{(self.content1.rating + self.content2.rating) / 2}\n')
            self.assertEqual(
                lines[2], f'{self.channel2.title},{float(self.content3.rating)}\n')
