from django.contrib import admin
from .models import *


class ContentInline(admin.TabularInline):
    model = Content
    fk_name = 'channel'
    extra = 0


class SubChannelInline(admin.TabularInline):
    model = Channel
    fk_name = 'parent_channel'
    extra = 0


@admin.register(Channel)
class ChannelAdmin(admin.ModelAdmin):
    inlines = (ContentInline, SubChannelInline)
    list_display = ('title', 'language')


@admin.register(Content)
class ContentAdmin(admin.ModelAdmin):
    list_display = ('title', 'rating', 'description')


@admin.register(File)
class FileAdmin(admin.ModelAdmin):
    list_display = ('content', 'file_type')
