from django.db import models


class Content(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField(null=True, blank=True)
    metadata = models.JSONField(default=dict, null=True, blank=True)
    rating = models.DecimalField(
        max_digits=3, decimal_places=1, default=None, null=True, blank=True)
    channel = models.ForeignKey(
        'Channel', on_delete=models.CASCADE, related_name='contents', null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self) -> str:
        return self.title
