from django.db import models
from channels.utils import get_channel_rating


class Channel(models.Model):
    title = models.CharField(max_length=255)
    language = models.CharField(max_length=255)
    picture = models.ImageField(upload_to='channel_pictures/')
    parent_channel = models.ForeignKey(
        'self', on_delete=models.CASCADE, related_name='subchannels', null=True, blank=True)

    def __str__(self) -> str:
        return self.title

    @property
    def has_subchannels(self) -> bool:
        return self.subchannels.exists()

    @property
    def has_contents(self) -> bool:
        return self.contents.exists()

    @property
    def rating(self) -> float:
        return get_channel_rating(self)

    def save(self, *args, **kwargs):
        if self.id and self.has_subchannels and self.has_contents:
            raise ValueError(
                "A channel cannot have both subchannels and contents.")
        elif self.id and not (self.has_subchannels and self.has_contents):
            raise ValueError(
                "A channel must have at least one subchannel or content.")
        super().save(*args, **kwargs)
