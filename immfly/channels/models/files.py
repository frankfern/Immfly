from django.db import models


class File(models.Model):
    content = models.ForeignKey(
        'Content', on_delete=models.CASCADE, related_name='files')
    url = models.FileField(upload_to='content_files/')
    file_type = models.CharField(max_length=10, choices=[
        ('video', 'Video'),
        ('pdf', 'PDF'),
        ('text', 'Text')
    ])

    def __str__(self) -> str:
        return self.content.__str__() + ' - ' + self.file_type
