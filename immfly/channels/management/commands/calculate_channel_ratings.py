import csv
from django.core.management.base import BaseCommand
from channels.models import Channel
from channels.utils import get_channel_rating


class Command(BaseCommand):
    help = 'Calculate and export channel ratings to a CSV file'

    def handle(self, *args, **options):
        channels = Channel.objects.all()

        ratings = {}
        for channel in channels:
            rating = get_channel_rating(channel)
            ratings[channel.title] = rating

        with open('channel_ratings.csv', mode='w', newline='') as file:
            writer = csv.writer(file)
            writer.writerow(['Channel Title', 'Average Rating'])
            for title, rating in ratings.items():
                writer.writerow([title, rating])
