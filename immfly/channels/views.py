from rest_framework import viewsets, mixins, status
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response

from .models import Channel, File, Content
from .serializers import ChannelSerializer, ContentSerializer


class ChannelViewset(mixins.RetrieveModelMixin,
                     mixins.ListModelMixin,
                     viewsets.GenericViewSet):

    serializer_class = ChannelSerializer
    ordering_fields = '__all__'
    ordering = ['title']
    filterset_fields = ['title', 'parent_channel', 'language']
    model = Channel

    def get_queryset(self):
        parent_channel = self.request.query_params.get('parent_channel')
        filters = {}
        if not parent_channel:
            filters["parent_channel__isnull"] = True
        return Channel.objects.filter(**filters)


class ContentViewset(mixins.RetrieveModelMixin,
                     mixins.ListModelMixin,
                     viewsets.GenericViewSet):

    serializer_class = ContentSerializer
    ordering_fields = '__all__'
    ordering = ['title']
    filterset_fields = ['title', 'channel']
    model = Content

    def dispatch(self, request, *args, **kwargs):
        channel_id = kwargs['channel_id']
        self.channel = get_object_or_404(Channel, pk=channel_id)
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return Content.objects.filter(channel=self.channel)
