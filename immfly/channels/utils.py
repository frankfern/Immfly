

def get_channel_rating(channel) -> float:
    if channel.has_subchannels:
        subchannel_ratings = []
        for subchannel in channel.subchannels.all():
            subchannel_rating = get_channel_rating(subchannel)
            subchannel_ratings.append(subchannel_rating)
        return sum(subchannel_ratings) / len(subchannel_ratings)
    elif channel.has_contents:
        content_ratings = [content.rating for content in channel.contents.all(
        ) if content.rating is not None]
        if not content_ratings:
            return 0
        return sum(content_ratings) / len(content_ratings)
    else:
        return 0
