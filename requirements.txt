# Base
Pillow==8.3.2

# Django
Django==4.2

# Django REST Framework
djangorestframework
djangorestframework-simplejwt
django-filter

# Passwords security
argon2-cffi

# Environment
django-environ

# Code quality
autopep8

# Tools
django-extensions

gunicorn

# Static files
whitenoise

#BD
psycopg2-binary
mypy