# Immfly API

This API is designed for a media platform and was developed using the Python programming language and the Django and Django Rest Framework libraries.

## Overview

Immfly API allows us to display media contents organized by channels.

## Software Requirements

- [python 3.11+](https://www.python.org/)
- [Django 4.2+](https://www.djangoproject.com/download/)
- [Django Rest Framework 3.14+](http://www.django-rest-framework.org/#installation)


### Running the Project

```bash
$ docker-compose build
$ docker-compose up
```

## About the Solution

As a solution, the Content, Channels, and Files classes have been defined to store the data of the audiovisual platform. By executing the calculate_channel_ratings command, we will obtain a CSV file listing the ratings of all channels." 

## License

[This project is under MIT License](https://opensource.org/licenses/MIT)
